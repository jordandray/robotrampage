using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RobotRampage.Desktop
{
    public static class WeaponManager
    {
        #region Declarations
        public static List<Particle> Shots = new List<Particle>();
        public static Texture2D Texture;
        public static Rectangle shotRectangle = 
            new Rectangle(0, 128, 32, 32);
        public static float WeaponSpeed = 600f;

        private static float shotTimer = 0f;
        private static float shotMinTimer = 0.15f;

        private static float rocketMinTimer = 0.5f;

        public enum WeaponType
        {
            Normal,
            Triple,
            Rocket
        };

        public static WeaponType CurrentWeaponType = WeaponType.Normal;
        public static float WeaponTimeRemaining = 0.0f;

        private static float weaponTimeDefault = 30.0f;
        private static float tripleWeaponSplitAngle = 15;
        
        public static List<Sprite> PowerUps = new List<Sprite>();

        private static int maxActivePowerUps = 5;
        private static float timeSinceLastPowerUp = 0.0f;
        private static float timeBetweenPowerUps = 2.0f;
        private static Random rand = new Random();

        #endregion Declarations
        
        #region Properties

        public static bool CanFireWeapon
        {
            get { return (shotTimer >= WeaponFireDelay); }
        }

        public static float WeaponFireDelay
        {
            get
            {
                if (CurrentWeaponType == WeaponType.Rocket)
                {
                    return rocketMinTimer;
                }
                else
                {
                    return shotMinTimer;
                }
            }
        }
        #endregion Properties
        
        #region Effects Management Methods

        private static void AddShot(
            Vector2 location,
            Vector2 velocity,
            int frame)
        {
            Particle shot = new Particle(
                location,
                Texture,
                shotRectangle,
                velocity,
                Vector2.Zero,
                400f,
                120,
                Color.White,
                Color.White);
            
            shot.AddFrame(new Rectangle(
                shotRectangle.X + shotRectangle.Width,
                shotRectangle.Y,
                shotRectangle.Width,
                shotRectangle.Height));

            shot.Animate = false;
            shot.Frame = frame;
            shot.RotateTo(velocity);
            Shots.Add(shot);
        }

        private static void createLargeExplosion(Vector2 location)
        {
            EffectsManager.AddLargeExploson(
                location + new Vector2(-10, -10));
            EffectsManager.AddLargeExploson(
                location + new Vector2(-10, 10));
            EffectsManager.AddLargeExploson(
                location + new Vector2(10, 10));
            EffectsManager.AddLargeExploson(
                location + new Vector2(10, -10));
            EffectsManager.AddLargeExploson(location);
        }
        #endregion Effects Management Methods
        
        #region Weapon Management Methods

        public static void FireWeapon(Vector2 location, Vector2 velocity)
        {
            switch (CurrentWeaponType)
            {
                case WeaponType.Normal:
                    AddShot(location, velocity, 0);
                    break;
                
                case WeaponType.Triple:
                    AddShot(location, velocity, 0);

                    float baseAngle = (float) Math.Atan2(
                        velocity.Y,
                        velocity.X);

                    float offset = MathHelper.ToRadians(
                        tripleWeaponSplitAngle);
                    
                    AddShot(
                        location,
                        new Vector2(
                            (float) Math.Cos(baseAngle - offset),
                            (float) Math.Sin(baseAngle - offset)
                            ) * velocity.Length(),
                        0);
                    
                    AddShot(
                        location,
                        new Vector2(
                        (float) Math.Cos(baseAngle + offset),
                        (float) Math.Sin(baseAngle + offset)
                        ) * velocity.Length(),
                        0);
                    break;
                
                case WeaponType.Rocket:
                    AddShot(location, velocity, 1);
                    break;
            }

            shotTimer = 0.0f;
        }

        private static void checkWeaponUpgradeExpire(float elapsed)
        {
            if (CurrentWeaponType != WeaponType.Normal)
            {
                WeaponTimeRemaining -= elapsed;
                if (WeaponTimeRemaining <= 0)
                {
                    CurrentWeaponType = WeaponType.Normal;
                }
            }
        }

        private static void tryToSpawnPowerUp(int x, int y, WeaponType type)
        {
            if (PowerUps.Count >= maxActivePowerUps)
            {
                return;
            }

            Rectangle thisDestination =
                TileMap.SquareWorldRectangle(new Vector2(x, y));

            foreach (var powerUp in PowerUps)
            {
                if (powerUp.WorldRectangle == thisDestination)
                {
                    return;
                }
            }

            if (!(PathFinder.FindPath(
                new Vector2(x, y),
                Player.PathingNodePosition) == null))
            {
                Sprite newPowerUp = new Sprite(
                    new Vector2(thisDestination.X, thisDestination.Y),
                    Texture,
                    new Rectangle(64, 128, 32, 32),
                    Vector2.Zero);

                newPowerUp.Animate = false;
                newPowerUp.CollisionRadius = 14;
                newPowerUp.AddFrame(new Rectangle(96, 128, 32, 32));

                if (type == WeaponType.Rocket)
                {
                    newPowerUp.Frame = 1;
                }
                PowerUps.Add(newPowerUp);
                timeSinceLastPowerUp = 0.0f;
            }
        }

        private static void checkPowerUpSpawns(float elapsed)
        {
            timeSinceLastPowerUp += elapsed;
            
            if (timeSinceLastPowerUp >= timeBetweenPowerUps)
            {
                WeaponType type = WeaponType.Triple;
                if (rand.Next(0, 2) == 1)
                {
                    type = WeaponType.Rocket;
                }
                
                tryToSpawnPowerUp(
                    rand.Next(0, TileMap.MapWidth),
                    rand.Next(0, TileMap.MapHeight),
                    type);
            }
        }
        #endregion Weapon Management Methods
        
        #region Update and Draw

        public static void Update(GameTime gameTime)
        {
            float elapsed = (float) gameTime.ElapsedGameTime.TotalSeconds;
            shotTimer += elapsed;
            checkWeaponUpgradeExpire(elapsed);

            for (var x = Shots.Count - 1; x >= 0; x--)
            {
                Shots[x].Update(gameTime);
                
                checkShotWallImpacts(Shots[x]);
                checkShotEnemyImpacts(Shots[x]);
                
                if (Shots[x].Expired)
                {
                    Shots.RemoveAt(x);
                }
            }
            
            checkPowerUpSpawns(elapsed);
            checkPowerUpPickups();
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            foreach (var sprite in Shots)
            {
                sprite.Draw(spriteBatch);
            }

            foreach (var sprite in PowerUps)
            {
                sprite.Draw(spriteBatch);
            }
        }
        #endregion Update and Draw
        
        #region Collision Detection

        private static void checkShotWallImpacts(Sprite shot)
        {
            if (shot.Expired)
            {
                return;
            }

            if (TileMap.IsWallTile(
                TileMap.GetSquareAtPixel(shot.WorldCenter)))
            {
                shot.Expired = true;

                if (shot.Frame == 0)
                {
                    EffectsManager.AddSparksEffect(
                        shot.WorldCenter,
                        shot.Velocity);
                }
                else
                {
                    createLargeExplosion(shot.WorldCenter);
                    checkRocketSplashDamage(shot.WorldCenter);
                }
            }
        }

        private static void checkPowerUpPickups()
        {
            for (var x = PowerUps.Count - 1; x >= 0; x--)
            {
                if (Player.BaseSprite.IsCircleColliding(
                    PowerUps[x].WorldCenter,
                    PowerUps[x].CollisionRadius))
                {
                    switch (PowerUps[x].Frame)
                    {
                        case 0:
                            CurrentWeaponType = WeaponType.Triple;
                            break;
                        
                        case 1:
                            CurrentWeaponType = WeaponType.Rocket;
                            break;
                    }

                    WeaponTimeRemaining = weaponTimeDefault;
                    PowerUps.RemoveAt(x);
                }
            }
        }

        private static void checkShotEnemyImpacts(Sprite shot)
        {
            if (shot.Expired)
            {
                return;
            }

            foreach (var enemy in EnemyManager.Enemies)
            {
                if (!enemy.Destroyed)
                {
                    if (shot.IsCircleColliding(
                        enemy.EnemyBase.WorldCenter,
                        enemy.EnemyBase.CollisionRadius))
                    {
                        shot.Expired = true;
                        enemy.Destroyed = true;
                        GameManager.Score += 10;

                        if (shot.Frame == 0)
                        {
                            EffectsManager.AddExplosion(
                                enemy.EnemyBase.WorldCenter,
                                enemy.EnemyBase.Velocity / 30);
                        }
                        else
                        {
                            if (shot.Frame == 1)
                            {
                                createLargeExplosion((shot.WorldCenter));
                                checkRocketSplashDamage(shot.WorldCenter);
                            }
                        }
                    }
                }
            }
        }

        private static void checkRocketSplashDamage(Vector2 location)
        {
            int rocketSplashRadius = 40;

            foreach (var enemy in EnemyManager.Enemies)
            {
                if (!enemy.Destroyed)
                {
                    if (enemy.EnemyBase.IsCircleColliding(
                        location, rocketSplashRadius))
                    {
                        enemy.Destroyed = true;
                        GameManager.Score += 10;
                        EffectsManager.AddExplosion(
                            enemy.EnemyBase.WorldCenter,
                            Vector2.Zero);
                    }
                }
            }
        }
        #endregion Collision Detection
    }
}